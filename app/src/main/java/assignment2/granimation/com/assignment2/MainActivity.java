package assignment2.granimation.com.assignment2;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


@TargetApi(Build.VERSION_CODES.CUPCAKE)
public class MainActivity extends Activity implements SensorEventListener {
    //Variables
    private SensorManager senSensorManager;
    private Sensor senAccelerometer;
    private long lastUpdate = 0;
    private float last_x, last_y, last_z;
    private static final int SHAKE_THRESHOLD = 400;
    private ArrayList<String> favourites = new ArrayList<String>();
    private ArrayList<String> history = new ArrayList<String>();
    private String tab1Name = getString(R.string.foodTab);
    private String tab2Name = getString(R.string.favouriteTab);
    private String tab3Name = getString(R.string.logTab);
    private String tab4Name = getString(R.string.submitTab);
    private String recipeURL = "";
    private String recipeID = "";
    private String recipeImageSubstring1 = "http://hemb.priv.no/privat/randomDinner.php?dinnerID=";
    private String recipeImageSubstring2 = "&showImage=";
    private boolean inFoodTab = true;
    private boolean canShake = true;
    GridImageViewAdapter gridImageViewAdapter = new GridImageViewAdapter(this);


    //Objects
    private TextView txtShakeNotifier;
    private ImageButton btnAddFavourite;
    private ImageButton btnGenerate;
    private WebView webRecipe;
    private ImageButton btnTest;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Lock to portrait orientation due to shaking
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //Set up tabs
        TabHost tabHost = (TabHost) findViewById(R.id.tabHost);
        tabHost.setup();

        TabHost.TabSpec tabSpec = tabHost.newTabSpec(getString(R.string.foodTab));
        tabSpec.setContent(R.id.tabRandom);
        tabSpec.setIndicator(getString(R.string.foodTab));
        tabHost.addTab(tabSpec);

        tabSpec = tabHost.newTabSpec(getString(R.string.favouriteTab));
        tabSpec.setContent(R.id.tabFavourites);
        tabSpec.setIndicator(getString(R.string.favouriteTab));
        tabHost.addTab(tabSpec);

        tabSpec = tabHost.newTabSpec(getString(R.string.logTab));
        tabSpec.setContent(R.id.tabHistory);
        tabSpec.setIndicator(getString(R.string.logTab));
        tabHost.addTab(tabSpec);

        tabSpec = tabHost.newTabSpec(getString(R.string.submitTab));
        tabSpec.setContent(R.id.tabSubmit);
        tabSpec.setIndicator(getString(R.string.submitTab));
        tabHost.addTab(tabSpec);

        //Set up accelerometer sensor
        senSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        senAccelerometer = senSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        senSensorManager.registerListener(this, senAccelerometer , SensorManager.SENSOR_DELAY_NORMAL);

        //Set up objects
        txtShakeNotifier = (TextView) findViewById(R.id.txtShakeNotifier);
        btnAddFavourite = (ImageButton) findViewById(R.id.btnAddFavourite);
        btnGenerate = (ImageButton) findViewById(R.id.btnGenerate);


        webRecipe = (WebView) findViewById(R.id.webRecipe);
        webRecipe.getSettings().setJavaScriptEnabled(true);
        webRecipe.setBackgroundColor(0);
        GridView grdHistory = (GridView) findViewById(R.id.grdHistory);
        grdHistory.setAdapter(gridImageViewAdapter);


        //Set up onclick listeners
        txtShakeNotifier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getNewRecipe();
            }
        });

        btnAddFavourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (webRecipe.getUrl() == null) {
                    return;
                }

                //Check if recipe is previously favourited
                int foundAtIndex = findInFavorites(webRecipe.getUrl());


                String toastText = "";
                if (foundAtIndex >= 0) {
                    favourites.remove(foundAtIndex);
                    btnAddFavourite.setImageResource(android.R.drawable.btn_star_big_off);
                    toastText = getString(R.string.removeRecipieSuccess);
                } else {
                    favourites.add(webRecipe.getUrl());
                    btnAddFavourite.setImageResource(android.R.drawable.btn_star_big_on);

                }
                Toast.makeText(getApplicationContext(), toastText, Toast.LENGTH_SHORT).show();
            }
        });

        btnGenerate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getNewRecipe();


            }
        });

        //Make sure to only use the accelerometer when in the Food! tab.
        tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                if (tab1Name.equals(tabId)) {
                    inFoodTab = true;
                }
                else if (tab2Name.equals(tabId)) {
                    inFoodTab = false;
                }
                else if (tab3Name.equals(tabId)) {
                    inFoodTab = false;
                }
                else if (tab4Name.equals(tabId)) {
                    inFoodTab = false;
                }
            }
        });
    }

    //Get sensor data
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (!inFoodTab || !canShake) {
            return;
        }
        Sensor mySensor = sensorEvent.sensor;

        if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            float x = sensorEvent.values[0];
            float y = sensorEvent.values[1];
            float z = sensorEvent.values[2];

            long curTime = System.currentTimeMillis();


            if ((curTime - lastUpdate) > 200) {
                long diffTime = (curTime - lastUpdate);
                lastUpdate = curTime;


                float speed = Math.abs(x + y + z - last_x - last_y - last_z)/ diffTime * 10000;

                if (speed > SHAKE_THRESHOLD) {
                    getNewRecipe();
                }

                last_x = x;
                last_y = y;
                last_z = z;
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    protected void onPause() {
        super.onPause();
        senSensorManager.unregisterListener(this);
    }

    protected void onResume() {
        super.onResume();
        senSensorManager.registerListener(this, senAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    public void handleFavouriteIcon(int foundAtIndex) {
        if (foundAtIndex >= 0) {
            btnAddFavourite.setImageResource(android.R.drawable.btn_star_big_on);
        } else {
            btnAddFavourite.setImageResource(android.R.drawable.btn_star_big_off);
        }
    }

    public int findInFavorites(String url) {
        int foundAtIndex = -1;
        for (int i = 0; i < favourites.size(); i++) {
            if (favourites.get(i).equals(url)) {
                foundAtIndex = i;
                break;
            }
        }
        return foundAtIndex; //Will return -1 if not found
    }

    public void getNewRecipe() {
        //If this is not the first refresh, add previous page to the log
        if (!recipeURL.equals("")) {
            gridImageViewAdapter.historyPage.add(0, recipeURL);
            String imageURL = recipeImageSubstring1 + recipeID + recipeImageSubstring2 + recipeID;
            gridImageViewAdapter.historyImage.add(0, imageURL);
            //TODO: Add image url to gridImageViewAdapter
        }

        if (hasConnectivity()) {
            // Load the webpage when the device is connected to the network
            webRecipe.loadUrl("http://hemb.priv.no/privat/randomDinner.php");
            // The standard webpage redirects to a random recipe, and this code keeps it from starting up the browser
            webRecipe.setWebViewClient(new WebViewClient() {
                public boolean shouldOverrideUrlLoading(WebView view, String url){
                    recipeURL = url;
                    view.loadUrl(recipeURL);

                    // Get the last character in the url, which is the recipe ID
                    recipeID = recipeURL.substring(recipeURL.length() - 1);

                    // Check if this recipe is favourited and set correct star icon accordingly
                    int foundAtIndex = findInFavorites(recipeURL);
                    handleFavouriteIcon(foundAtIndex);


                    return false; // Return false so it is not handled by default action
                }
            });



            //btnAddFavourite.setEnabled(true);
            txtShakeNotifier.setVisibility(View.INVISIBLE);
            webRecipe.setVisibility(View.VISIBLE);

        } else {
            // set text to saved instance from the file, and add a message about the network
            txtShakeNotifier.setText(getString(R.string.noNetworkConnectionErrorMessage));
            //btnAddFavourite.setEnabled(false);
            txtShakeNotifier.setVisibility(View.VISIBLE);
            webRecipe.setVisibility(View.INVISIBLE);
        }
    }

    public boolean hasConnectivity() {
        ConnectivityManager connect = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connect != null) {
            NetworkInfo[] information = connect.getAllNetworkInfo();
            if (information != null) {
                for (int x = 0; x < information.length; x++) {
                    if (information[x].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.isChecked()) {
            item.setChecked(false);
            canShake = false;
        }
        else {
            item.setChecked(true);
            canShake = true;
        }

        return true;
    }
}
