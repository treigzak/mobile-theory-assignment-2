package assignment2.granimation.com.assignment2;

import java.net.URL;

/**
 * Created by Sindre on 24.09.2014.
 */
public class Recipe {
    private int _id;
    private URL _url;

    public int getId() {
        return _id;
    }

    public URL getUrl() {
        return _url;
    }

    /*
    private String _recipeName;
    private ArrayList<String> _ingredients = new ArrayList<String>();
    private ArrayList<String> _steps = new ArrayList<String>();


    public Recipe(int id, String recipeName, ArrayList<String> ingredients, ArrayList<String> steps) {
        _id = id;
        _recipeName = recipeName;
        _ingredients = ingredients;
        _steps = steps;
    }

    public String getRecipeName() {
        return _recipeName;
    }

    public ArrayList<String> getIngredients() {
        return _ingredients;
    }

    public ArrayList<String> getSteps() {
        return _steps;
    }
    */
}
