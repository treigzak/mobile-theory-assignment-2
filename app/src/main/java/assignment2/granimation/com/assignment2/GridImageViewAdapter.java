package assignment2.granimation.com.assignment2;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;


import java.util.ArrayList;

/**
 * Created by Sindre on 27.09.2014.
 */
public class GridImageViewAdapter extends BaseAdapter {
    private Context mContext;

    // Constructor
    public GridImageViewAdapter(Context c) {
        mContext = c;
    }

    public int getCount() {
        return mThumbIds.length;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        //TODO: Use this to get the returnvalue of the position of the recipe in the ArrayList for stuffs
        Toast.makeText(mContext, "You clicked on recipe #" + position, Toast.LENGTH_SHORT).show();
        return position;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {
            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams(150, 150));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(2, 2, 2, 2);
        } else {
            imageView = (ImageView) convertView;
        }

        imageView.setImageResource(mThumbIds[position]);
        //TODO: This shit below here instead
        //Picasso.with(mContext).load(historyImage.get(position)).resize(150, 150).centerCrop().into(imageView);
        return imageView;
    }

    public ArrayList<String> historyPage = new ArrayList<String>();
    public ArrayList<String> historyImage = new ArrayList<String>();

    // For testing, delete this shit once we get the recipe images properly
    public Integer[] mThumbIds = {
            R.drawable.no_image_set, R.drawable.no_image_set,
            R.drawable.no_image_set, R.drawable.no_image_set,
            R.drawable.no_image_set, R.drawable.no_image_set,
            R.drawable.no_image_set, R.drawable.no_image_set,
            R.drawable.no_image_set, R.drawable.no_image_set,
            R.drawable.no_image_set, R.drawable.no_image_set,
            R.drawable.no_image_set, R.drawable.no_image_set,
            R.drawable.no_image_set, R.drawable.no_image_set,
            R.drawable.no_image_set, R.drawable.no_image_set,
            R.drawable.no_image_set, R.drawable.no_image_set,
            R.drawable.no_image_set, R.drawable.no_image_set,
            R.drawable.no_image_set, R.drawable.no_image_set,
            R.drawable.no_image_set, R.drawable.no_image_set,
            R.drawable.no_image_set, R.drawable.no_image_set,
            R.drawable.no_image_set, R.drawable.no_image_set,
            R.drawable.no_image_set, R.drawable.no_image_set,
            R.drawable.no_image_set, R.drawable.no_image_set
    };
}
